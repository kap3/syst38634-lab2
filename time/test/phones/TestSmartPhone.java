package phones;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * 
 * @author Seby
 *
 */

public class TestSmartPhone {

	@Test
	public void testGetFormattedPriceRegular() {
		SmartPhone sp = new SmartPhone("iPhone", 1000, 3.0);
		String formattedPrice = sp.getFormattedPrice();
		assertTrue("The value provided does not match the result", formattedPrice.equals("$1,000"));
	}

	@Test
	public void testGetFormattedPriceException() {
		SmartPhone sp = new SmartPhone("iPhone", 1000, 3.0);
		String formattedPrice = sp.getFormattedPrice();
		assertFalse("The price provided doesn't match our database", formattedPrice.equals("$2,000"));
	}

	@Test
	public void testGetFormattedPriceBI() {
		SmartPhone sp = new SmartPhone("iPhone", 1000, 3.0);
		String formattedPrice = sp.getFormattedPrice();
		assertTrue("The price provided doesn't match the result", formattedPrice.equals("$1,000"));
	}

	@Test
	public void testGetFormattedPriceBO() {
		SmartPhone sp = new SmartPhone("iPhone", 1000, 3.0);
		String formattedPrice = sp.getFormattedPrice();
		assertFalse("The price provided doesn't match our database", formattedPrice.equals("$2,000"));
	}

	@Test
	public void testSetVersionRegular() throws VersionNumberException {

		SmartPhone sp = new SmartPhone("Iphone", 1000, 3.0);

		sp.setVersion(3.5);

		assertTrue("The version provided is not allowed", sp.getVersion() == 3.5);

	}

	@Test(expected = VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone sp = new SmartPhone("Iphone", 1000, 3.0);
		sp.setVersion(10.0);
		fail("The version provided is not allowed!");
	}

	@Test
	public void testSetVersionBI() throws VersionNumberException {
		SmartPhone sp = new SmartPhone("Iphone", 1000, 3.0);
		sp.setVersion(4.0);
		assertTrue("The version provided is not allowed", sp.getVersion() == 4.0);
	}

	@Test(expected = VersionNumberException.class)
	public void testSetVersionBO() throws VersionNumberException {
		SmartPhone sp = new SmartPhone("Iphone", 1000, 3.0);
		sp.setVersion(4.1);
		fail("The version provided is not allowed!");
	}

}
